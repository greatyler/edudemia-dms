<?php

//must populate the variable, so add dummy value until DMS needs this capability.
edudms_add_jquery_ui_page('test');

//get the pages to apply jquery_ui to
function edudms_jquery_ui_pages() {
	
	$jquipages = $GLOBALS["edudms_jquery_ui_pages"];

	return $jquipages;
}

//function that allows any plugin to add pages to the list of pages to apply jquery_ui
function edudms_add_jquery_ui_page($name) {
	if(empty($GLOBALS["edudms_jquery_ui_pages"])) {
	$GLOBALS["edudms_jquery_ui_pages"] = Array($name);
	}
	else {
	array_push($GLOBALS["edudms_jquery_ui_pages"], $name);
	}
}

$jquipages = edudms_jquery_ui_pages();
foreach ($jquipages as $jquipage) {
	global $pagenow;
	if($pagenow == $jquipage) {
		add_action( 'wp_enqueue_scripts','edudms_jquery_ui');
		add_action( 'admin_enqueue_scripts', 'edudms_jquery_ui' );
	}
}

function edudms_jquery_ui() {
	wp_enqueue_script('script', 'https://code.jquery.com/ui/1.12.1/jquery-ui.min.js','',1.0,true);
}



