<?php
/*
Plugin Name: Edudemia Department Management System
Plugin URI: https://bitbucket.org/greatyler/edudemia-dms
Bitbucket Plugin URI: greatyler/edudemia-dms
Description: Create a Department Management System
Version: 0.5.4
Author: Tyler Pruitt
Author URI:

Credits:
This plugin's director of development and primary developer is Tyler Pruitt.

The following plugins were sourced or referrenced in this project:
Code from:



Dependencies or Referrences:




License

Edudemia is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
any later version.

Edudemia is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.
 
See http://www.gnu.org/licenses/old_licenses/gpl_2.0.en.html.
*/



//include_once dirname(__FILE__) . '/edudms_templater.php';
include_once dirname(__FILE__) . '/tgmpa-plugin/plugin_functions.php';
include_once dirname(__FILE__) . '/plugins.php';
require_once dirname(__FILE__) . '/tgmpa-plugin/tgmpa-config.php';
include_once dirname(__FILE__) . '/tgmpa-plugin/edudms_plugins.php';
include_once dirname(__FILE__) . '/bootstrap.php';

function edudms_enqueue_admin_css() {
	wp_register_style( 'edudms_admin_css', plugins_url('css/edudms_admin_css.css',__FILE__ ));
    wp_enqueue_style( 'edudms_admin_css' );
}
add_action( 'admin_enqueue_scripts', 'edudms_enqueue_admin_css' );

function edudms_css_registration() {
	wp_register_style('edudms', plugins_url('css/edudms.css',__FILE__ ));
	wp_enqueue_style('edudms');
}
add_action( 'wp_enqueue_scripts','edudms_css_registration');


//edudms_make_template('profile_page.php', 'Profile Page', 'edudemia-dms');











?>