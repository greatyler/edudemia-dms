<?php

//must populate the variable, so add dummy value until DMS needs this capability.
edudms_add_bootstrap_page('test');

//get the pages to apply bootstrap to
function edudms_bootstrap_pages() {
	
	$bspages = $GLOBALS["edudms_bootstrap_pages"];

	return $bspages;
}

//function that allows any plugin to add pages to the list of pages to apply bootstrap
function edudms_add_bootstrap_page($name) {
	if(empty($GLOBALS["edudms_bootstrap_pages"])) {
	$GLOBALS["edudms_bootstrap_pages"] = Array($name);
	}
	else {
	array_push($GLOBALS["edudms_bootstrap_pages"], $name);
	}
}

$bspages = edudms_bootstrap_pages();
foreach ($bspages as $bspage) {
	global $pagenow;
	if($pagenow == $bspage) {
		add_action( 'wp_enqueue_scripts','edudms_bootstrap');
		add_action( 'admin_enqueue_scripts', 'edudms_bootstrap' );
	}
}

function edudms_bootstrap() {
	wp_register_style('edudms_bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css','',1.0,true);
	wp_enqueue_style('edudms_bootstrap','',1.0,true);
	wp_enqueue_script('script', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js','',1.0,true);
}



